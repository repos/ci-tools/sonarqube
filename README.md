# GitLab CI template for SonarQube

This project implements a GitLab CI/CD template to continuously inspect Wikimedia Gitlab projects with [SonarQube](https://docs.sonarqube.org/latest/). This is an initiative of the [Code Health Group](https://www.mediawiki.org/wiki/Code_Health_Group/projects/Code_Health_Metrics)

## Usage

In order to include this template in your java project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: "repos/ci-tools/sonarqube"
    ref: main # use any branch, tag, or commit hash here
    file: "templates/maven-sonar-ci.yml"
```

This adds a `sonar-scanner` job to the pipeline that runs at `test` stage. You may customize that further in your `.gitlab-ci.yml`, for example:

```yaml

variables:
  MAVEN_OPTS: -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository

# […]

mvn-package:
  # […]
  artifacts:
    name: $CI_JOB_STAGE-$CI_COMMIT_REF_NAME-target
    paths:
      - target/*
    expire_in: 7 days

  cache:
    - key: maven
      policy: pull-push
      paths:
        - .m2/repository
  
  script:
    - ./mvnw --batch-mode package

# […]

sonar-scanner:
  cache:
  - key: maven
    policy: pull
    paths:
      - .m2/repository
  variables:
    SONAR_MAVEN_BINARY: ./mvnw --batch-mode # use local wrapper for better control over maven version
    SONAR_MAVEN_GOALS: sonar:sonar # save time, by default this would re-run test and verify goals
  needs:
    - job: mvn-package
      artifacts: true # re-use job artefacts when not executing the test goal
  tags:
    - wmcs
```
Adding (skip sonar) or (sonar skip) in the comment section of your commit allows you to skip running sonar in the pipeline when sonar feedback is not needed, or when sonar has an issue and you need to urgently merge something:
### Example
```yaml
This is a commit message for your patch or MR

(skip sonar)

Bug: #
```




Note: This is a WIP.
